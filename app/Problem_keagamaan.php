<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problem_keagamaan extends Model
{
  protected $table = 'problem_keagamaan';
  protected $guarded = [];
  protected $dates = ['deleted_at'];
}
