<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data_keagamaan as data;
use Yajra\Datatables\Datatables;
use DB;

class DataController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $reqest) {
        $data = data::join('sub-district', 'data_keagamaan.id_kecamatan', '=', 'sub-district.id')
        ->join('districts', 'sub-district.sub', '=','districts.id')
        ->join('provinces','districts.sub', '=', 'provinces.id')->get();

        // dd($data[0]);

        return view('admin/data', compact(['data']));
    }

    public function anyData($tahun) {
        // DB::statement(DB::raw('set @rownum=0'));
        // $data = data::select([
        //     DB::raw('@rownum  := @rownum  + 1 AS rownum'),
        //     'id',
        //     'nama_kelurahan',
        //     'jumlah_penduduk',
        //     'tahun',
        //     'islam']);
        // $datatables = Datatables::of($data);
        //
        // if ($keyword = $request->get('search')['value']) {
        //     $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        // }

        // return $datatables->make(true);

        // return \Datatables::of(data::query())->make(true);
        $data = data::where('tahun',$tahun);
    }

    public function edit($id) {
        $data = data::find($id);

        $current_year = date('Y');
        $range = range($current_year, $current_year-10);
        $years = array_combine($range, $range);
        if (($key = array_search($data->tahun, $years)) !== false) {
            unset($years[$key]);
        }
        array_unshift($years, $data->tahun);

        // dd($years);

        return view('admin/edata', compact(['data','years']));
    }

    public function sedit(Request $request) {
        $data = array('nama_kelurahan'=>$request->kelurahan, 'jumlah_penduduk'=>$request->penduduk,'islam'=>$request->islam,'kristen_protestan'=>$request->protestan, 'kristen_katolik'=>$request->katolik,'hindu'=>$request->hindu,'budha'=>$request->budha, 'konghucu' => $request->konghucu, 'jml_masjid' => $request->masjid, 'pesantren' => $request->pesantren, 'jml_dai' => $request->dai, 'gereja' => $request->gereja, 'pura' => $request->pura, 'kelenteng' => $request->kelenteng, 'keterangan' => $request->keterangan, 'tahun' => $request->tahun);
        if (DB::table('data_keagamaan')->where('id',$request->data)->update($data)) {
            return back();
        }
    }

    public function tambah(Request $request) {
        $data = array('nama_kelurahan'=>$request->kelurahan, 'jumlah_penduduk'=>$request->penduduk,'islam'=>$request->islam,'kristen_protestan'=>$request->protestan, 'kristen_katolik'=>$request->katolik,'hindu'=>$request->hindu,'budha'=>$request->budha, 'konghucu' => $request->konghucu, 'jml_masjid' => $request->masjid, 'pesantren' => $request->pesantren, 'jml_dai' => $request->dai, 'gereja' => $request->gereja, 'pura' => $request->pura, 'kelenteng' => $request->kelenteng, 'keterangan' => $request->keterangan, 'tahun' => $request->tahun, 'id_kecamatan' => '2');
        if (data::create($data)) {
            return redirect('/data')->with('success','Post Berhasil ditambahkan');
        }
    }
}
