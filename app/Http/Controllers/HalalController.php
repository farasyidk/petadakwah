<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data_keagamaan as data;
use Yajra\Datatables\Datatables;
use DB;

class HalalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      $data = data::join('sub-district', 'data_keagamaan.id_kecamatan', '=', 'sub-district.id')
          ->join('districts', 'sub-district.sub', '=','districts.id')
          ->join('provinces','districts.sub', '=', 'provinces.id')->get();

          // dd($data[0]);

          return view('admin/halal', compact(['data']));
    }
}
