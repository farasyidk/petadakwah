var lokasi = {lat: -7.0378, lng: 111.0611};
var markers = new Array();
markers['organisasi'] = new Array();
markers['problem'] = new Array();
var Lat = 0, Lng =0;
var map;
function initMap() {

  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: lokasi,
    mapTypeId: 'roadmap'
  });
  infoWindow = new google.maps.InfoWindow;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {

      lokasi = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }

  map.setCenter(lokasi);
}

function ambil(get) {
  $("document").ready(function(){
    var data = {
      test: $( "#test" ).val()
    };
    var options = {
      url: "/datas/"+get,
      dataType: "json",
      type: "GET",
      data: { test: JSON.stringify( data ) }, // Our valid JSON string
      success: function( data, status, xhr ) {
        var datas;
          for (var i = 0; i < data['message'].length; i++) {
            datas = data['message'][i];
            addMarker(data['type'], datas);
          }

      },
      error: function( xhr, status, error ) {
          console.log("huu "+error);
      }
    };
    $.ajax( options );
  });
}

function addMarker(icn, datas) {
  // console.log(datas);
    var contentString;
    var posisi ={lat: datas['latitude'], lng: datas['longitude']};
    posisi.lat = +posisi.lat; posisi.lng = +posisi.lng;

    switch (icn) {
      case 'organisasi':
          icon = 'img/collaboration.png';
          contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">'+datas["nama_organisasi"]+'</h1>'+
                '<div id="bodyContent">'+
                '<p><b>'+datas["lokasi"]+', </b>'+datas["keterangan"]+'</p>'+
                '</div>'+
                '</div>';
      break;
      case 'problem':
          icon = 'img/problem.png';
          contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">'+datas["nama_problem"]+'</h1>'+
                '<div id="bodyContent">'+
                '<p><b>'+datas["lokasi_problem"]+', </b>'+datas["uraian_problem"]+'</p>'+
                '</div>'+
                '</div>';
      break;
      default:

    }

    var infowindow = new google.maps.InfoWindow({
      content: contentString,
    });

  var marker = new google.maps.Marker({
    position: posisi,
    map: map,
    icon: icon
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  markers[icn].push(marker);
}

function deleteMarkers(mark) {
    for (var i = 0; i < markers[mark].length; i++) {
        markers[mark][i].setMap(null);
    }

    markers[mark] = new Array();
}
$(document).ready(function () {
  $('input').on('click',function () {
      if ($('#problem').is(':checked')) {
          ambil('problem');
      } else {
          deleteMarkers('problem');
      }

      if ($('#organisasi').is(':checked')) {
          ambil('organisasi');
      } else {
          deleteMarkers('organisasi');
      }
  });
});
