@extends('admin/master');

@section('content')
  <section class="py-5">
    <div class="row">
      <div class="col-lg-12 mb-5">
        <div class="card">
          <div class="card-header">
            <h3 class="h6 text-uppercase mb-0">Edit Data Keagamaan</h3>
          </div>
          <div class="card-body">
            {!! Form::open(array('route'=>'edata','files'=>true)) !!}
              <input type="hidden" name="data" value="{{$data->id}}">
              <input type="hidden" name="_method" value="PUT">
              {{csrf_field()}}
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Nama Kelurahan</label>
                <div class="col-md-9">
                  <input type="text" placeholder="Nama Kelurahan" value="{{$data->nama_kelurahan}}" name="kelurahan" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Penduduk</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Penduduk" value="{{$data->jumlah_penduduk}}" name="penduduk" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Islam</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Islam" value="{{$data->islam}}" name="islam" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kristen Protestan</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah kristen protestan" value="{{$data->kristen_protestan}}" name="protestan" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kristen katolik</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah kristen katolik" value="{{$data->kristen_katolik}}" name="katolik" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah hindu</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah hindu" value="{{$data->hindu}}" name="hindu" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah budha</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah budha" value="{{$data->budha}}" name="budha" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah konghucu</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah konghucu" value="{{$data->konghucu}}" name="konghucu" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah masjid</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah masjid" value="{{$data->jml_masjid}}" name="masjid" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah pesantren</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah pesantren" value="{{$data->pesantren}}" name="pesantren" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Dai</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Dai" value="{{$data->jml_dai}}" name="dai" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Gereja</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Gereja" value="{{$data->gereja}}" name="gereja" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Pura</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Pura" value="{{$data->pura}}" name="pura" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kelenteng</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Kelenteng" value="{{$data->kelenteng}}" name="kelenteng" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Keterangan</label>
                <div class="col-md-9">
                  <textarea name="keterangan" class="form-control">{{$data->keterangan}}</textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Tahun</label>
                <div class="col-md-9 select mb-3">
                  <select name="tahun" class="form-control">
                    @foreach ($years as $y)
                      <option value="{{$y}}">{{$y}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="line"></div>
              {{-- <div class="form-group row has-success">
                <label class="col-sm-3 form-control-label">Input with success</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control is-valid">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row has-danger">
                <label class="col-sm-3 form-control-label">Input with error</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control is-invalid">
                  <div class="invalid-feedback ml-3">Please provide your name.</div>
                </div>
              </div>
              <div class="line"></div> --}}
              <div class="form-group row">
                <div class="col-md-9 ml-auto">
                  <button type="reset" class="btn btn-secondary">Cancel</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
