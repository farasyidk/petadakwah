@extends('admin/master');

@section('content')
  <section class="py-5">
    <div class="row">
      <div class="col-lg-12 mb-5">
        <div class="card">
          <div class="card-header">
            <h3 class="h6 text-uppercase mb-0">Edit Data Keagamaan</h3>
          </div>
          <div class="card-body">
            {!! Form::open(array('route'=>'tdata','files'=>true)) !!}
            {{csrf_field()}}

              <div class="form-group row">
                <label class="col-md-3 form-control-label">Nama Kelurahan</label>
                <div class="col-md-9">
                  <input type="text" placeholder="Nama Kelurahan" name="kelurahan" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Penduduk</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Penduduk" name="penduduk" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Islam</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Islam" name="islam" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kristen Protestan</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah kristen protestan" name="protestan" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kristen katolik</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah kristen katolik" name="katolik" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah hindu</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah hindu" name="hindu" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah budha</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah budha" name="budha" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah konghucu</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah konghucu" name="konghucu" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah masjid</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah masjid" name="masjid" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah pesantren</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah pesantren" name="pesantren" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Dai</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Dai" name="dai" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Gereja</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Gereja" name="gereja" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Pura</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Pura" name="pura" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Jumlah Kelenteng</label>
                <div class="col-md-9">
                  <input type="number" placeholder="Jumlah Kelenteng" name="kelenteng" required class="form-control">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Keterangan</label>
                <div class="col-md-9">
                  <textarea name="keterangan" class="form-control"></textarea>
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">Tahun</label>
                <div class="col-md-9 select mb-3">
                  <select name="tahun" class="form-control">
                    @php
                      $current_year = date('Y');
                      $range = range($current_year, $current_year-10);
                      $years = array_combine($range, $range);
                    @endphp
                    @foreach ($years as $y)
                      <option value="{{$y}}">{{$y}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="line"></div>
              {{-- <div class="form-group row has-success">
                <label class="col-sm-3 form-control-label">Input with success</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control is-valid">
                </div>
              </div>
              <div class="line"></div>
              <div class="form-group row has-danger">
                <label class="col-sm-3 form-control-label">Input with error</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control is-invalid">
                  <div class="invalid-feedback ml-3">Please provide your name.</div>
                </div>
              </div>
              <div class="line"></div> --}}
              <div class="form-group row">
                <div class="col-md-9 ml-auto">
                  <button type="submit" class="btn btn-secondary">Cancel</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
