@extends('admin/master')

@section('style')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css">
  <style media="screen">
    .form-group {
      margin-bottom: 0
    }
  </style>
@endsection

@section('jscript')
  <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
      $('#data').DataTable({
        initComplete: function () {
            this.api().columns([3]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($( "#tahunn" ))
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
            this.api().columns([4]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($( "#kecamatan" ))
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, true )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
            this.api().columns([5]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($( "#kabupaten" ))
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
            this.api().columns([6]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($( "#provinsi" ))
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
        // processing: true,
        // serverSide: true,
        // ajax: 'https://datatables.yajrabox.com/eloquent/row-num-data',
        // columns: [
        //     // or just disable search since it's not really searchable. just add searchable:false
        //     {data: 'rownum', name: 'rownum'},
        //     {data: 'id', name: 'id'},
        //     {data: 'name', name: 'name'},
        //     {data: 'email', name: 'email'},
        //     {data: 'created_at', name: 'created_at'}
        //
        // ]
        // "order": [[ 0, "desc" ]]
      });
  });
  function edit(id) {
    window.location.href="/edata";
  }
  </script>

@endsection

@section('content')


  <section class="py-5">
    <div class="row">
      <div class="col-lg-12 mb-8">
        <div class="card">
          <div class="card-header">
            <h6 class="text-uppercase mb-0">Data Keagamaan</h6>
          </div>
          <div class="card-body">
            @if (auth()->user()->isAdmin >= 2)
              <div class="form-group row">
                <label class="col-md-2 form-control-label">Kecamatan</label>
                <div class="col-md-5 select mb-3" id="kecamatan">
                </div>
              </div>
            @endif
            @if (auth()->user()->isAdmin >= 3)
              <div class="form-group row">
                <label class="col-md-2 form-control-label">Kabupaten</label>
                <div class="col-md-5 select mb-3" id="kabupaten">
                </div>
              </div>
            @endif
            @if (auth()->user()->isAdmin >= 4)
              <div class="form-group row">
                <label class="col-md-2 form-control-label">Provinsi</label>
                <div class="col-md-5 select mb-3" id="provinsi">
                </div>
              </div>
            @endif
            <div class="form-group row">
              <label class="col-md-2 form-control-label">Tahun</label>
              <div class="col-md-5 select mb-3" id="tahunn">
                {{-- <select id="years" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Pilih tahun..">
                  @php
                    $current_year = date('Y');
                    $range = range($current_year, $current_year-10);
                    $years = array_combine($range, $range);
                  @endphp
                  @foreach ($years as $y)
                    <option value="{{$y}}">{{$y}}</option>
                  @endforeach
                </select> --}}
              </div>
            </div>
            <div class="input-group input-group-sm float-right" style="width:170px; margin-bottom:10px; padding-right:0">
              <button type="button" onclick="window.location.href='/tdata'" class="btn btn-default"><i class="glyphicon glyphicon-plus" style="margin-right:5px;"></i>Tambah Data</button>
            </div>
            <table id="data" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="users-table_info">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Kelurahan</th>
                  <th>Jumlah Penduduk</th>
                  <th>Tahun</th>
                  @if (auth()->user()->isAdmin >= 2)
                    <th>Kecamatan</th>
                  @endif
                  @if (auth()->user()->isAdmin >= 3)
                    <th>Kabupaten</th>
                  @endif
                  @if (auth()->user()->isAdmin >= 4)
                    <th>Provinsi</th>
                  @endif
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @php
                  $i = 1;
                @endphp
                @foreach ($data as $dt)
                  <tr>
                    <td>{{$i}}</td>
                    <td>{{$dt->nama_kelurahan}}</td>
                    <td>{{$dt->jumlah_penduduk}}</td>
                    <td>{{$dt->tahun}}</td>
                    @if (auth()->user()->isAdmin >= 2)
                      <td>{{$dt->kecamatan}}</td>
                    @endif
                    @if (auth()->user()->isAdmin >= 3)
                      <td>{{$dt->kabupaten}}</td>
                    @endif
                    @if (auth()->user()->isAdmin >= 4)
                      <td>{{$dt->provinsi}}</td>
                    @endif
                    <td>
                      {!! Form::open(['url'=>''])!!}
                      {{csrf_field()}}
                      <a href="/edata/{{$dt->id}}" class="btn btn-warning btn-sm" title="Lihat atau Edit">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                      </a>
                      <input type="hidden" name="_method" value="DELETE">
                      <button onClick="hapus({{$dt->id}})" class="btn btn-danger btn-sm" title="Hapus">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </button>
                      {!!Form::close()!!}
                    </td>
                  </tr>
                  @php
                    $i++;
                  @endphp
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </section>
@endsection
