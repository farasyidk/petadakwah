<?php
Route::get('/', function () {
    return view('layouts/master');
});
Route::get('/datas/{get?}', 'GetController@datas');
Route::get('/data', function () {
    return view('admin/data');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'HomeController@admin')->middleware('admin');
// Route::controller('data', 'DataController', [
//     'anyData'  => 'data.datas',
//     'index' => 'data',
// ])->middleware('admin');
Route::get('/data', 'DataController@index')->middleware('admin');
Route::get('/data/{tahun?}', 'DataController@anyData')->name('data/get');
Route::PUT('/edata', ['as' => 'edata', 'uses' => 'DataController@sedit'])->middleware('admin');
Route::get('/edata/{id?}', 'DataController@edit')->middleware('admin');
Route::view('/tdata/', 'admin.tdata')->middleware('admin');
Route::POST('/edata', ['as' => 'tdata', 'uses' => 'DataController@tambah'])->middleware('admin');
Route::get('/organisasi', 'OrganisasiController@index')->middleware('admin');
Route::get('/problem', 'ProblemController@index')->middleware('admin');
Route::get('/halal', 'HalalController@index')->middleware('admin');
